<?php
/*
*	Package:		PHPCrazy
*	Link:			http://zhangyun.org/
*	Author: 		Crazy <mailzhangyun@qq.com>
*	Copyright:		2014-2015 Crazy
*	License:		Please read the LICENSE file.
*/ include T('header'); ?>

		<div class="main">
  		<div class="am-u-lg-6 am-u-md-8 am-u-sm-centered">
			<?php if ($submit): include T('error_box'); endif; ?>

			<?php if ($continue_finish): ?>
				<div class="row">
					<p class="text-success"><?php echo L('用户资料已保存'); ?></p>
				</div>
			<?php endif; ?>

			<div class="row">
				<h1><?php echo L('修改账号信息'); ?></h1>
				<hr />
				<form action="<?php echo HomeUrl('index.php/user:EditUserProfile/'); ?>" method="post" class="am-form">
						<label for="inputUsername"><?php echo L('用户名'); ?></label>
							<div><input id="inputUsername" type="text" name="username" value="<?php echo $UserInfo['username']; ?>" /></div>
							<div><p class="text-info text-small"><?php echo L('输入用户名说明'); ?></p></div>
						<label for="inputEmail"><?php echo L('邮箱'); ?></label>
							<div><input id="inputEmail" type="text" name="email" value="<?php echo $UserInfo['email']; ?>" /></div>
						<label for="inputPassword1"><?php echo L('新密码'); ?></label>
							<div><input id="inputPassword1" type="password" name="password1" value="" /></div>
							<div><p class="text-info text-small"><?php echo L('输入密码说明'); ?></p></div>
						<label for="inputPassword2"><?php echo L('确认新密码'); ?></label>
							<div><input id="inputPassword2" type="password" name="password2" value="" /></div>
							<div><p class="text-info text-small"><?php echo L('输入确认密码说明'); ?></p></div>
						<label for="inputPassword"><?php echo L('密码'); ?></label>
							<div><input id="inputPassword" type="password" name="password" value="" /></div>
							<div><p class="text-info text-small"><?php echo L('输入密码'); ?></p></div>
						<a class="am-btn am-btn-default am-btn-sm am-fr" href="<?php echo HomeUrl('index.php/main:user/'); ?>">&laquo;<?php echo L('返回上级'); ?></a>
						<input type="submit" class="am-btn am-btn-primary am-btn-sm am-fl" name="submit" value="<?php echo L('保存'); ?>" />
						<br/>
				</form>
			</div>
		</div>
		</div>
<?php include T('footer'); ?>