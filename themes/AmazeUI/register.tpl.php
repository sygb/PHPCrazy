<?php
/*
*	Package:		PHPCrazy
*	Link:			http://zhangyun.org/
*	Author: 		Crazy <mailzhangyun@qq.com>
*	Copyright:		2014-2015 Crazy
*	License:		Please read the LICENSE file.
*/ include T('header'); ?>

<?php if ($submit): include T('error_box'); endif; ?>

			<div class="am-g">
				<div class="am-u-lg-6 am-u-md-8 am-u-sm-centered">
			    	<hr />
					<h3><?php echo L('注册'); ?></h3>
			    	<hr />
					<form action="<?php echo HomeUrl('index.php/main:login/?action=register'); ?>" method="post"  class="am-form">
						
						<label for="inputUsername"><?php echo L('用户名'); ?></label>
						<br /><input id="inputUsername" type="text" name="username" value="" placeholder="<?php echo L('输入用户名'); ?>" />
						<p class="text-info text-small"><?php echo L('输入用户名说明'); ?></p>
						
						<label for="inputEmail"><dd class="left"><?php echo L('邮箱'); ?></dd></label>
						<br /><input id="inputEmail" type="text" name="email" value="" placeholder="example@domain.com" />
						<p class="text-info text-small"><?php echo L('输入邮箱说明'); ?></p>

						<label for="inputPassword1"><?php echo L('密码'); ?></label>
						<br /><input id="inputPassword1" type="password" name="password1" value="" placeholder="***" />
						<p class="text-info text-small"><?php echo L('输入密码说明'); ?></p>

						<label for="inputPassword2"><?php echo L('确认密码'); ?></label>
						<br /><input id="inputPassword2" type="password" name="password2" value="" placeholder="***" />
						<p class="text-info text-small"><?php echo L('输入确认密码说明'); ?></p>

						<label for="inputcaptcha"><?php echo L('验证码'); ?></label>
						<a href="<?php echo HomeUrl('index.php/main:login/?action=register'); ?>" title="<?php echo L('刷新'); ?>">
						<img src="<?php echo HomeUrl('Captcha.php'); ?>" />
						</a>
						<br /><input id="inputcaptcha" type="text" name="captcha" value="" />
						<p class="text-info text-small"><?php echo L('输入验证码 说明'); ?></p>

						<div class="am-cf">
							<a class="am-btn am-btn-default am-btn-sm am-fr" href="<?php echo HomeUrl('index.php/main:login/'); ?>">&laquo;<?php echo L('登录'); ?></a>
							<input type="submit" name="submit" value="<?php echo L('完成注册'); ?>" class="am-btn am-btn-primary am-btn-sm am-fl am-btn-default"/>
						</div>

					</form>
				</div>
			</div>

<?php include T('footer'); ?>