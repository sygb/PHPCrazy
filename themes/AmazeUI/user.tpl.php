<?php
/*
*	Package:		PHPCrazy
*	Link:			http://zhangyun.org/
*	Author: 		Crazy <mailzhangyun@qq.com>
*	Copyright:		2014-2015 Crazy
*	License:		Please read the LICENSE file.
*/ include T('header'); ?>
<style>
.am-panel-bd {
padding: 0px;
}
</style>
		<div class="main">
  		<div class="am-u-lg-6 am-u-md-8 am-u-sm-centered">
  			<hr>
  			<div class="am-panel-primary">
			  <div class="am-panel-hd">用户资料</div>
			  <div class="am-panel-bd">
				<ul class="am-list am-list-static am-list-border">
					<li><?php echo L('ID'); ?>：<?php echo $GLOBALS['U']['id']; ?></li>
					<li><?php echo L('用户名'); ?>：<?php echo $GLOBALS['U']['username']; ?></li>
					<li><?php echo L('邮箱'); ?>：<?php echo $GLOBALS['U']['email']; ?></li>
					<li><?php echo L('注册时间'); ?>：<?php echo date('Y-m-d H:i', $GLOBALS['U']['regtime']); ?></li>
				</ul>
			  </div>
			</div>
				<li class="bd-b-line block"><a class="block" href="<?php echo HomeUrl('index.php/user:EditUserProfile/'); ?>"><h2><?php echo L('修改账号信息'); ?></h2></a></li>
				<?php if ($Auth[ADMIN]): ?>
				<li class="bd-b-line block"><a class="block" href="<?php echo HomeUrl(ADMIN_FILE); ?>"><h2><?php echo L('网站后台管理'); ?></h2></a></li>
				<?php endif; ?>
			</div>
		</div>
<?php include T('footer'); ?>