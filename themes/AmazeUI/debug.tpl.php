<?php
/*	
*	Package:		PHPCrazy
*	Link:			http://zhangyun.org/
*	Author: 		Crazy <mailzhangyun@qq.com>
*	Copyright:		2014-2015 Crazy
*	License:		Please read the LICENSE file.
*/ include T('header'); ?>
		<div class="main">
			<div class="row">
				<h1><?php echo $msg; ?></h1>
				<dl class="lr">
					<dd class="left"><strong><?php echo L('行'); ?></strong></dd>
					<dt class="left"><?php echo $line; ?></dt>
				</dl>
				<dl class="lr">
					<dd class="left"><strong><?php echo L('文件'); ?></strong></dd>
					<dt class="left"><?php echo $file; ?></dt>
				</dl>
			</div>
		</div>
<?php include T('footer'); ?>