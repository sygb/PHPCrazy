<?php
/*
*	Package:		PHPCrazy
*	Link:			http://zhangyun.org/
*	Author: 		Crazy <mailzhangyun@qq.com>
*	Copyright:		2014-2015 Crazy
*	License:		Please read the LICENSE file.
*/
include T('header'); ?>
	
	<?php if ($submit): include T('error_box'); endif; ?>

	<div class="am-g">
  		<div class="am-u-lg-6 am-u-md-8 am-u-sm-centered">
    		<hr />
      		<h3><?php echo L('登录'); ?></h3>
    		<hr />
    		<div class="am-btn-group">
      			<a href="#" class="am-btn am-btn-secondary am-btn-sm"><i class="am-icon-qq am-icon-sm"></i> Q Q</a>
      			<a href="#" class="am-btn am-btn-success am-btn-sm"><i class="am-icon-weixin am-icon-sm"></i> 微信</a>
      			<a href="#" class="am-btn am-btn-primary am-btn-sm"><i class="am-icon-weibo am-icon-sm"></i> 新浪</a>
    		</div>
    		<br />
    		<br />
    		<form action="<?php echo HomeUrl('index.php/main:login/'); ?>" method="post" class="am-form">
      			<label for="email"><?php echo L('帐号'); ?>:</label>
      			<input id="inputAccount" type="text" name="account" value="" placeholder="<?php echo L('邮箱 ID 用户名'); ?>" />
      			<br />
      			
      			<label for="password"><?php echo L('密码'); ?>:</label>
      				<input id="inputPassword" type="password" name="password" value="" placeholder="<?php echo L('输入密码'); ?>" />
      				<br />
      				<div class="am-cf">
        				<input type="submit" name="submit" value="<?php echo L('登录'); ?>" class="am-btn am-btn-primary am-btn-sm am-fl">
        				<a class="am-btn am-btn-default am-btn-sm am-fr" href="<?php echo HomeUrl('index.php/main:login/?action=forgetpassword'); ?>"><?php echo L('忘记密码'); ?>？</a>
      				</div>
    		</form>
  		</div>
	</div>
<?php include T('footer'); ?>