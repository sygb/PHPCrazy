<?php
/*
*	Package:		PHPCrazy
*	Link:			http://zhangyun.org/
*	Author: 		Crazy <mailzhangyun@qq.com>
*	Copyright:		2014-2015 Crazy
*	License:		Please read the LICENSE file.
*
*	这是一个分页模版, 不能直接 include T('pagination');
*	如果您想进一步了解这是怎么工作的
*	请打开文件 indcludes/lib/class.pagination.php
*/ exit; ?>

<!-- BEGIN Tag --><button type="button" class="btn btn-default" >{TAG}</button><!-- END Tag -->

<!-- BEGIN FirstOrLast --><a href="{URL}" class="btn btn-default" >{TAG}</a><!-- END FirstOrLast -->

<!-- BEGIN NextOrPrev --><a href="{URL}" class="btn btn-default" >{TAG}</a><!-- END NextOrPrev -->

<!-- BEGIN Info --><button type="button" class="btn btn-success" >{ONPAGE} / {TOTAL}</button><!-- END Info -->

<!-- BEGIN Box --><div class="row">{FIRST}{NEXT}{INFO}{PREV}{LAST}</div><!-- END Box -->