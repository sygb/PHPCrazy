<?php
/*
*	  Package:		PHPCrazy
*	  Link:			http://zhangyun.org/
*	  Author:		Crazy <mailzhangyun@qq.com>
*	  Copyright:	2014-2015 Crazy
*	  License:		Please read the LICENSE file.
*/ ?>
      	<div data-am-widget="navbar" class="am-navbar am-cf am-navbar-default " id="">
          	<ul class="am-navbar-nav am-cf am-avg-sm-4">
              	<li data-am-navbar-share>
                  	<a href="###">
                      	<span class="am-icon-share-square-o"></span>
                      	<span class="am-navbar-label"><?php echo L('分享'); ?></span>
                  	</a>
              	</li>
              	<li data-am-navbar-qrcode>
                  	<a href="###">
                      	<span class="am-icon-qrcode"></span>
                      	<span class="am-navbar-label"><?php echo L('二维码'); ?></span>
                  	</a>
              	</li>
              	<li>
                  	<a href="https://git.oschina.net/Crazy-code/PHPCrazy">
                      	<span class="am-icon-github"></span>
                      	<span class="am-navbar-label"><?php echo L('GitHub'); ?></span>
                  	</a>
              	</li>
              	<li>
                  	<a href="http://zhangyun.org/">
                      	<span class="am-icon-download"></span>
                      	<span class="am-navbar-label"><?php echo L('下载使用'); ?></span>
                  	</a>
              	</li>
              	<li>
                  	<a href="http://git.oschina.net/Crazy-code/PHPCrazy/issues">
                      	<span class="am-icon-location-arrow"></span>
                     	<span class="am-navbar-label"><?php echo L('Bug 反馈'); ?></span>
                  	</a>
              	</li>
          	</ul>
      	</div>
      	<footer data-am-widget="footer" class="am-footer am-footer-default" data-am-footer="{  }">
          	<div class="am-footer-switch">
              	<a href="<?php echo HomeUrl(); ?>"><?php echo L('首页'); ?></a>
              	<span class="am-footer-divider">|</span>
              	<?php if ($GLOBALS[ 'U'][ 'login']): ?>
                  	<a href="<?php echo HomeUrl('index.php/main:user/'); ?>"><?php echo $GLOBALS[ 'U'][ 'username']; ?></a>
                  	<span class="am-footer-divider">|</span>
                  	<a href="<?php echo HomeUrl('index.php/main:login/?action=logout'); ?>"><?php echo L( '注销'); ?></a>
              	<?php else: ?>
                  	<a href="<?php echo HomeUrl('index.php/main:login/'); ?>"><?php echo L( '登录'); ?></a>
                  	<span class="am-footer-divider">|</span>
                  	<a href="<?php echo HomeUrl('index.php/main:login/?action=register'); ?>"><?php echo L( '注册'); ?></a>
              	<?php endif; ?>
          	</div>
          	<div class="am-footer-miscs ">
              	<p><?php echo sprintf(L('版权所有 年 作者'), date('Y'), $GLOBALS['C']['sitename']); ?></p>
          	</div>
      	</footer>
	</body>
</html>