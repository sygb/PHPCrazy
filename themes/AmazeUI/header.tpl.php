<?php
/*
*	Package:		PHPCrazy
*	Link:			http://zhangyun.org/
*	Author: 		Crazy <mailzhangyun@qq.com>
*	Copyright:		2014-2015 Crazy
*	License:		Please read the LICENSE file.
*/ echo '<?xml version="1.0" encoding="utf-8"?>'; ?>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<meta http-equiv="Cache-Control" content="no-cache"/>
		<meta name="author" content="<?php echo $GLOBALS['C']['author']; ?>" />
		<link rel="icon" href="favicon.ico" type="image/x-icon" />
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
		<link rel="stylesheet" href="<?php echo HomeUrl('themes/AmazeUI/css/AmazeUI.css'); ?>"/>
		<script src="<?php echo HomeUrl('themes/AmazeUI/js/jquery.min.js'); ?>"/></script>
		<script src="<?php echo HomeUrl('themes/AmazeUI/js/amazeui.min.js'); ?>"/></script>
		<meta name="keywords" content="<?php echo $GLOBALS['C']['keywords']; ?>"/>
		<meta name="description" content="<?php echo $GLOBALS['C']['description']; ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<title><?php echo $GLOBALS['PageTitle']; ?></title>
	</head>
	<body>
        <header data-am-widget="header" class="am-header am-header-default">
            <div class="am-header-left am-header-nav">
                <a href="<?php echo HomeUrl(); ?>" class=""><i class="am-header-icon am-icon-home"></i></a>
            </div>
            <h1 class="am-header-title"><img src="<?php echo HomeUrl('themes/AmazeUI/images/logo.png'); ?>" /></h1>
            <div class="am-header-right am-header-nav">
                <a href="#right-link" class=""><i class="am-header-icon am-icon-bars"></i></a>
            </div>
        </header>
        <nav data-am-widget="menu" class="am-menu  am-menu-offcanvas1" data-am-menu-offcanvas>
            <a href="javascript: void(0)" class="am-menu-toggle"><i class="am-menu-toggle-icon"></i></a>
            <div class="am-offcanvas">
                <div class="am-offcanvas-bar">
                    <ul class="am-menu-nav sm-block-grid-1">
                        <li class="am-parent">
                            <a href="##"><?php echo $GLOBALS['U']['username']; ?></a>
                            <ul class="am-menu-sub am-collapse  sm-block-grid-2 ">
                            <?php if ($GLOBALS['U']['login']): ?>
                                <li><a href="<?php echo HomeUrl('index.php/main:login/?action=logout'); ?>"><?php echo L('注销'); ?></a></li>
                                <li class="am-menu-nav-channel"><a href="<?php echo HomeUrl('index.php/main:user/'); ?>" title="<?php echo L('用户中心'); ?>"><?php echo L('用户中心'); ?> &raquo;</a></li>
                            <?php else: ?>
                                <li><a href="<?php echo HomeUrl('index.php/main:login/'); ?>"><?php echo L('登录'); ?></a></li>
                                <li><a href="<?php echo HomeUrl('index.php/main:login/?action=register'); ?>"><?php echo L('注册'); ?></a></li>
                                <li><a href="<?php echo HomeUrl('index.php/main:login/?action=forgetpassword'); ?>"><?php echo L('忘记密码'); ?>?</a></li>
                            <?php endif; ?>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>