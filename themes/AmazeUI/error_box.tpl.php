<?php
/*	
*	Package:		PHPCrazy
*	Link:			http://zhangyun.org/
*	Author: 		Crazy <mailzhangyun@qq.com>
*	Copyright:		2014-2015 Crazy
*	License:		Please read the LICENSE file.
*/	if (!empty($error)): ?>
				<div class="row">
		<?php foreach ($error as $key => $msg): ?>
					<p class="text-warning"><?php echo $msg; ?></p>
		<?php endforeach; ?>
				</div>
	<?php endif; ?>