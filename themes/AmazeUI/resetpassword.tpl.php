<?php
/*
*	Package:		PHPCrazy
*	Link:			http://zhangyun.org/
*	Author: 		Crazy <mailzhangyun@qq.com>
*	Copyright:		2014-2015 Crazy
*	License:		Please read the LICENSE file.
*/ include T('header'); ?>
		<div class="main">
  			<div class="am-u-lg-6 am-u-md-8 am-u-sm-centered">
				<h1><?php echo L('重置密码'); ?></h1>
				<hr />
				<form action="login.php?action=resetpassword&key=<?php echo $activation_key; ?>" method="post">
					<p><?php sprintf(L('问候 用户名'), $username) ;?></p>
					<dl class="lr">
						<label for="inputPassword1"><dd class="left"><?php echo L('密码'); ?></dd></label>
						<dt class="left">
							<div><input id="inputPassword1" class="form-control" type="password" name="password1" value="" placeholder="***" /></div>
							<div><p class="text-info text-small"><?php echo L('输入密码说明'); ?></p></div>
						</dt>
					</dl>
					<dl class="lr">
						<label for="inputPassword2"><dd class="left"><?php echo L('确认密码'); ?></dd></label>
						<dt class="left">
							<div><input id="inputPassword2" class="form-control" type="password" name="password2" value="" placeholder="***" /></div>
							<div><p class="text-info text-small"><?php echo L('输入确认密码说明'); ?></p></div>
						</dt>
					</dl>
					<dl class="lr">
						<dd class="left"><a href="./">&laquo;<?php echo L('首页'); ?></a></dd>
						<dt class="left"><input type="submit" class="btn btn-default" name="submit" value="<?php echo L('确认'); ?>" /></dt>
					</dl>
				</form>
			</div>
		</div>
<?php include T('footer'); ?>