<?php
/*	
*	Package:		PHPCrazy
*	Link:			http://zhangyun.org/
*	Author: 		Crazy <mailzhangyun@qq.com>
*	Copyright:		2014-2015 Crazy
*	License:		Please read the LICENSE file.
*/ include T('header'); ?>
				
				<?php if ($submit): include T('error_box'); endif; ?>

				<?php if ($finish): ?>
					<div class="row">
						<p class="text-success"><?php echo sprintf(L('激活邮件发送 邮箱 说明'), '（<span class="text-large">' . $email . '</span>）'); ?></p>
					</div>
				<?php endif; ?>

			<div class="am-g">
				<div class="am-u-lg-6 am-u-md-8 am-u-sm-centered">
			    	<hr />
					<h3><?php echo L('找回密码'); ?></h3>
			    	<hr />
					<form action="login.php?action=forgetpassword" method="post" class="am-form">
						<label for="inputEmail"><?php echo L('邮箱'); ?></label>
						<br /><input id="inputEmail" class="form-control" type="text" name="email" value="" placeholder="example@domain.com" />
						<p class="text-info text-small"><?php echo L('输入邮箱地址'); ?></p>

						<div class="am-cf">
							<a class="am-btn am-btn-default am-btn-sm am-fr" href="<?php echo HomeUrl('index.php/main:login/'); ?>">&laquo;<?php echo L('登录'); ?></a>
							<input type="submit" name="submit" value="<?php echo L('发送邮件'); ?>" class="am-btn am-btn-primary am-btn-sm am-fl am-btn-default"/>
						</div>
					</form>
				</div>
			</div>

<?php include T('footer'); ?>