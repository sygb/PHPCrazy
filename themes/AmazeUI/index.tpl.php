<?php
/*
*	Package:		PHPCrazy
*	Link:			http://zhangyun.org/
*	Author: 		Crazy <mailzhangyun@qq.com>
*	Copyright:		2014-2015 Crazy
*	License:		Please read the LICENSE file.
*
*	index 页面主题模版
*
*/ include T('header'); ?>
<style>

.am-gallery li{
	text-align: center;
}
.box-shadow{
	-moz-box-shadow:0px 0px 6px #999;
    -webkit-box-shadow:0px 0px 6px #999;
    box-shadow:0px 0px 6px #999;
}
.am-titlebar-default {
    border-bottom: 0px solid #DEDEDE;
}
.am-navbar-nav a [class*="am-icon"] {
    display: block !important;
}
</style>
<div class="am-u-lg-6 am-u-md-8 am-u-sm-centered">
<div class="am-alert am-alert-danger am-margin-horizontal-sm" data-am-alert>
  <button type="button" class="am-close">&times;</button>
  <h3><?php echo L('您好，站长！'); ?></h3>
				<p><?php echo sprintf(L('首页安装完成说明'), '<code>install</code>'); ?></p>
				<p><?php echo sprintf(L('首页模版位置说明'), '<strong>./theme/Simple/index.tpl.php</strong>'); ?></p>
				<p><?php echo sprintf(L('首页后台入口说明'), '<a href="' . HomeUrl() . 'admin.php">' . HomeUrl() . 'admin.php</a>'); ?></p>
</div>

<!-- Slider -->
<div data-am-widget="slider" class="am-slider am-slider-c3" data-am-slider='{&quot;controlNav&quot;:false}'>
  <ul class="am-slides">
    <li>
      <img src="http://cn.bing.com/az/hprichv/LondonTrainStation_GettyRR_139321755_ZH-CN742316019.jpg">
      <div class="am-slider-desc">
        <div class="am-slider-counter">
          <span class="am-active">1</span>/4</div>远方 有一个地方 那里种有我们的梦想</div>
    </li>
    <li>
      <img src="http://s.cn.bing.net/az/hprichbg/rb/CardinalsBerries_ZH-CN10679090179_1366x768.jpg">
      <div class="am-slider-desc">
        <div class="am-slider-counter">
          <span class="am-active">2</span>/4</div>某天 也许会相遇 相遇在这个好地方</div>
    </li>
    <li>
      <img src="http://s.cn.bing.net/az/hprichbg/rb/QingdaoJiaozhou_ZH-CN10690497202_1366x768.jpg">
      <div class="am-slider-desc">
        <div class="am-slider-counter">
          <span class="am-active">3</span>/4</div>不要太担心 只因为我相信 终会走过这条遥远的道路</div>
    </li>
    <li>
      <img src="http://s.cn.bing.net/az/hprichbg/rb/FennecFox_ZH-CN13720911949_1366x768.jpg">
      <div class="am-slider-desc">
        <div class="am-slider-counter">
          <span class="am-active">4</span>/4</div>OH PARA PARADISE 是否那么重要 你是否那么地遥远</div>
    </li>
  </ul>
</div>

<ul data-am-widget="gallery" class="am-gallery am-avg-sm-4
  am-avg-md-4 am-avg-lg-4 am-gallery-default box-shadow" data-am-gallery="{ pureview: true }">
  <li>
    <div class="am-gallery-item">
      <a href="#">
        <span class="am-icon-comments-o am-icon-lg"></span>
        <h3 class="am-gallery-title">社区</h3>
      </a>
    </div>
  </li>
  <li>
    <div class="am-gallery-item">
      <a href="#">
        <span class="am-icon-cloud-download am-icon-lg"></span>
        <h3 class="am-gallery-title">下载</h3>
      </a>
    </div>
  </li>
  <li>
    <div class="am-gallery-item">
      <a href="#">
        <span class="am-icon-puzzle-piece am-icon-lg"></span>
        <h3 class="am-gallery-title">帮助</h3>
      </a>
    </div>
  </li>
  <li>
    <div class="am-gallery-item">
      <a href="#">
        <span class="am-icon-th-large am-icon-lg"></span>
        <h3 class="am-gallery-title">应用</h3>
      </a>
    </div>
  </li>
</ul>

<div data-am-widget="titlebar" class="am-titlebar am-titlebar-default">
  <h2 class="am-titlebar-title">栏目标题</h2>
  <nav class="am-titlebar-nav">
    <a href="#more" class="">more &raquo;</a>
  </nav>
</div>
<div data-am-widget="list_news" class="am-list-news am-list-news-default">
  <!--列表标题-->


  <div class="am-list-news-bd">
    <ul class="am-list">
    					<?php foreach (L('首页测试文章 数组') as $title): ?>
      <li class="am-g am-list-item-dated">
        <a href="##" class="am-list-item-hd"><?php echo $title; ?></a>
        <span class="am-list-date">2013-09-18</span>
      </li>
      					<?php endforeach; ?>
    </ul>
  </div>
</div>
<div data-am-widget="slider" class="am-slider am-slider-default" data-am-slider='{&quot;animation&quot;:&quot;slide&quot;,&quot;animationLoop&quot;:false,&quot;itemWidth&quot;:200,&quot;itemMargin&quot;:5}'>
  <ul class="am-slides">
    <li>
      <img src="http://cn.bing.com/az/hprichv/LondonTrainStation_GettyRR_139321755_ZH-CN742316019.jpg">
    </li>
    <li>
      <img src="http://s.cn.bing.net/az/hprichbg/rb/CardinalsBerries_ZH-CN10679090179_1366x768.jpg">
    </li>
    <li>
      <img src="http://s.cn.bing.net/az/hprichbg/rb/QingdaoJiaozhou_ZH-CN10690497202_1366x768.jpg">
    </li>
    <li>
      <img src="http://s.cn.bing.net/az/hprichbg/rb/FennecFox_ZH-CN13720911949_1366x768.jpg">
    </li>
    <li>
      <img src="http://cn.bing.com/az/hprichv/LondonTrainStation_GettyRR_139321755_ZH-CN742316019.jpg">
    </li>
    <li data-thumb="http://s.cn.bing.net/az/hprichbg/rb/CardinalsBerries_ZH-CN10679090179_1366x768.jpg">
      <img src="http://s.cn.bing.net/az/hprichbg/rb/CardinalsBerries_ZH-CN10679090179_1366x768.jpg">
    </li>
    <li>
      <img src="http://s.cn.bing.net/az/hprichbg/rb/QingdaoJiaozhou_ZH-CN10690497202_1366x768.jpg">
    </li>
    <li>
      <img src="http://s.cn.bing.net/az/hprichbg/rb/FennecFox_ZH-CN13720911949_1366x768.jpg">
    </li>
  </ul>
</div>
</div>
<?php include T('footer'); ?>