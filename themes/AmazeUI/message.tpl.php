<?php
/*	
*	Package:		PHPCrazy
*	Link:			http://zhangyun.org/
*	Author: 		Crazy <mailzhangyun@qq.com>
*	Copyright:		2014-2015 Crazy
*	License:		Please read the LICENSE file.
*/ include T('header'); ?>

<?php if ($level == SUCCESS): ?>
				<div class="row">
					<p class="text-success"><?php echo $message; ?></p>
				</div>
<?php elseif ($level == INFO): ?>
				<div class="row">
					<p class="text-info"><?php echo $message; ?></p>
				</div>
<?php elseif ($level == WARNING): ?>
				<div class="row">
					<p class="text-warning"><?php echo $message; ?></p>
				</div>
<?php elseif ($level == DANGER): ?>
				<div class="row">
					<p class="text-danger"><?php echo $message; ?></p>
				</div>
<?php else: ?>
				<div class="row">
					<p class="text-info"><?php echo $message; ?></p>
				</div>
<?php endif; ?>
<?php include T('footer'); ?>