<?php
/*
*	Package:		PHPCrazy
*	Link:			http://zhangyun.org/
*	Author: 		Crazy <mailzhangyun@qq.com>
*	Copyright:		2014-2015 Crazy
*	License:		Please read the LICENSE file.
*/

if (!defined('IN_PHPCRAZY')) exit;

LoadFunc('admin');

require Lang('admin');

$A = new Auth();

$Auth = $A->IsAuth($U['id']);

if (!$Auth[ADMIN]) {

	header('Location: '.HomeUrl('index.php/main:login/'));

	AppEnd();

}
