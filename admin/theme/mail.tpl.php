<?php
/*
*	Package:		PHPCrazy
*	Link:			http://zhangyun.org/
*	Author: 		Crazy <mailzhangyun@qq.com>
*	Copyright:		2014-2015 Crazy
*	License:		Please read the LICENSE file.
*/ include T('admin_header', true); ?>
		<div class="main">
			<div class="row">
				<h1 class="text-center"><?php echo L('邮件设置'); ?></h1>
			</div>
			<?php if ($submit): include T('error_box'); endif; ?>
			<form action="<?php echo AdminUrl('mail'); ?>" method="post">
				<div class="row">
					<dl class="lr">
						<label for="Selsend_mail"><dd class="left"><?php echo L('邮件发送'); ?></dd></label>
						<dt class="left">
							<select id="Selsend_mail" name="send_mail">
								<option value="1"<?php echo $send_mail_on; ?>><?php echo L('开启'); ?></option>
								<option value="0"<?php echo $send_mail_off; ?>><?php echo L('关闭'); ?></option>
							</select>
							<p class="text-info text-small"><?php echo L('邮件发送 说明'); ?></p>
						</dt>
					</dl>
					<dl class="lr">
						<label for="SelSmtp"><dd class="left"><?php echo L('使用SMTP发送邮件'); ?></dd></label>
						<dt class="left">
							<select id="SelSmtp" name="smtp">
								<option value="1"<?php echo $smtp_on; ?>><?php echo L('开启'); ?></option>
								<option value="0"<?php echo $smtp_off; ?>><?php echo L('关闭'); ?></option>
							</select>
							<p class="text-info text-small"><?php echo L('使用SMTP发送邮件 说明'); ?></p>
						</dt>
					</dl>
					<dl class="lr">
						<label for="inputsystem_mail"><dd class="left"><?php echo L('系统邮件地址'); ?></dd></label>
						<dt class="left"><input id="inputsystem_mail" type="text" name="system_mail" value="<?php echo $GLOBALS['C']['system_mail']; ?>" /></dt>
					</dl>
					<dl class="lr">
						<label for="Selsmtp_secure"><dd class="left"><?php echo L('使用SSL发送邮件'); ?></dd></label>
						<dt class="left">
							<select id="Selsmtp_secure" name="smtp_secure">
								<option value="1"<?php echo $smtp_secure_on; ?>><?php echo L('开启'); ?></option>
								<option value="0"<?php echo $smtp_secure_off; ?>><?php echo L('关闭'); ?></option>
							</select>
						</dt>
					</dl>
						
				</div>
				<div class="row">
					<h2><?php echo L('SMTP信息'); ?></h2>
					<dl class="lr">
						<label for="inputsmtp_host"><dd class="left"><?php echo L('SMTP服务器'); ?></dd></label>
						<dt class="left">
							<input id="inputsmtp_host" type="text" name="smtp_host" value="<?php echo $GLOBALS['C']['smtp_host']; ?>" placeholder="例如：smtp.domain.com" />
							<p class="text-info text-small"><?php echo L('SMTP服务器 说明'); ?></p>
						</dt>
					</dl>
					<dl class="lr">
						<label for="inputsmtp_port"><dd class="left"><?php echo L('SMTP端口'); ?></dd></label>
						<dt class="left">
							<input id="inputsmtp_port" type="text" name="smtp_port" value="<?php echo $GLOBALS['C']['smtp_port']; ?>" placeholder="25" />
							<p class="text-info text-small"><?php echo L('SMTP端口 说明'); ?></p>
						</dt>
					</dl>
					<dl class="lr">
						<label for="inputsmtp_username"><dd class="left"><?php echo L('SMTP用户'); ?></dd></label>
						<dt class="left">
							<input id="inputsmtp_username" type="text" name="smtp_username" value="<?php echo $GLOBALS['C']['smtp_username']; ?>" placeholder="user" />
							<p class="text-info text-small"><?php echo L('SMTP用户 说明'); ?></p>
						</dt>
					</dl>
					<dl class="lr">
						<label for="inputsmtp_password"><dd class="left"><?php echo L('SMTP密码'); ?></dd></label>
						<dt class="left">
							<input id="inputsmtp_password" type="text" name="smtp_password" value="<?php echo $GLOBALS['C']['smtp_password']; ?>" placeholder="user" />
							<p class="text-info text-small"><?php echo L('SMTP密码 说明'); ?></p>
						</dt>
					</dl>
				</div>
				<div class="row">
					<dl class="lr">
						<dd class="left"><a href="admin.php">&laquo;<?php echo L('返回上级'); ?></a></dd>
						<dt class="left"><input type="submit" name="submit" value="<?php echo L('保存'); ?>" /></dt>
					</dl>
				</div>
			</form>
		</div>

<?php include T('admin_footer', true); ?>