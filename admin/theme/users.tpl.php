<?php
/*
*	Package:		PHPCrazy
*	Link:			http://zhangyun.org/
*	Author: 		Crazy <mailzhangyun@qq.com>
*	Copyright:		2014-2015 Crazy
*	License:		Please read the LICENSE file.
*/ include T('admin_header', true); ?>

		<div class="main">
			<div class="row">
				<h1 class="text-center"><?php echo L('用户列表'); ?></h1>
			</div>
			<div class="row">
				<form action="<?php echo AdminUrl('users&mode=search'); ?>" method="post">
					<dl class="lr">
						<dd class="left"><input type="text" class="form-control" name="k" value="" placeholder="<?php echo L('邮箱 ID 用户名'); ?>" /></dd>
						<dt class="right"><input type="submit" class="btn btn-default" value="<?php echo L('搜索用户'); ?>"></dt>
					</dl>
				</form>			
			</div>
			<div class="row"><p class="text-success"><?php echo L('管理指定用户 说明'); ?></p></div>
			<div class="row">
				<ul>
					<?php foreach ($UserList as $User): ?>
					<li class="bd-b-line block">
						<a href="<?php echo AdminUrl('users&mode=info&user=' . $User['id']); ?>" class="block">
							<?php echo $User['username']; ?>
						</a>
					</li>
					<?php endforeach; ?>
				</ul>
			</div>
			<?php echo $P->Box(); ?>
		</div>

<?php include T('admin_footer', true); ?>