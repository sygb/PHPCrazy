<?php
/*
*	Package:		PHPCrazy
*	Link:			http://zhangyun.org/
*	Author: 		Crazy <mailzhangyun@qq.com>
*	Copyright:		2014-2015 Crazy
*	License:		Please read the LICENSE file.
*/ include T('admin_header', true); ?>

		<div class="main">
			<div class="row"><h1 class="text-center"><?php echo L('常规设置'); ?></h1></div>
			<?php if ($submit): include T('error_box'); endif; ?>
			<form action="<?php echo AdminUrl('config'); ?>" method="post">
				<div class="row">
					<h2><?php echo L('基本设置'); ?></h2>
					<dl class="lr">
						<label for="inputSiteTitle"><dd class="left"><?php echo L('网站标题'); ?></dd></label>
						<dt class="left"><input id="inputSiteTitle" class="form-control" type="text" name="sitename" value="<?php echo $GLOBALS['C']['sitename']; ?>" placeholder="<?php echo L('网站标题 说明'); ?>" /></dt>
					</dl>
					<dl class="lr">
						<label for="SelTheme"><dd class="left"><?php echo L('外观设置'); ?></dd></label>
						<dt class="left">
							<div><?php echo ThemeSel($GLOBALS['C']['theme'], 'name="theme" id="SelThemes" class="form-control"'); ?></div>
							<div><p class="text-info text-small"><?php echo L('主题设置 说明'); ?></p></div>
						</dt>
					</dl>
					<dl class="lr">
						<label for="timezone"><dd class="left"><?php echo L('时区设置'); ?></dd></label>
						<dt class="left">
							<div><?php echo Select($GLOBALS['lang']['timezone'], $GLOBALS['C']['timezone'], 'name="timezone" id="timezone" class="form-control"'); ?></div>
							<div><p class="text-info text-small"><?php echo sprintf(L('时区设置 说明'), $GLOBALS['lang']['timezone'][$GLOBALS['C']['timezone']], date($GLOBALS['C']['date_var'])); ?></p></div>
						</dt>
					</dl>
					<dl class="lr">
						<label for="inputDateVar"><dd class="left"><?php echo L('时间格式'); ?></dd></label>
						<dt class="left">
							<div><input id="inputDateVar" class="form-control" type="text" name="date_var" value="<?php echo $GLOBALS['C']['date_var']; ?>" placeholder="例如: Y-m-d H:i" /></div>
							<div><p class="text-info text-small"><?php echo L('时间格式 说明'); ?></p></div>
						</dt>
					</dl>
					<dl class="lr">
						<label for="inputLang"><dd class="left"><?php echo L('语言'); ?></dd></label>
						<dt class="left">
							<?php echo LangSel($GLOBALS['C']['lang'], 'id="inputLang" name="lang" class="form-control"'); ?>
						</dt>
					</dl>
					<dl class="lr">
						<label for="inputLang"><dd class="left"><?php echo L('HTTPS'); ?></dd></label>
						<dt class="left">
							<select name="http_secure" class="form-control">
								<option value="1"<?php echo $http_secure_on; ?>><?php echo L('开启'); ?></option>
								<option value="0"<?php echo $http_secure_off; ?>><?php echo L('关闭'); ?></option>
							</select>
							<p class="text-info text-small"><?php echo L('HTTPS 说明'); ?></p>
						</dt>
					</dl>				
				</div>
				<div class="row">
					<h2><?php echo L('SEO优化'); ?></h2>
					<dl class="lr">
						<label for="inputAuthor"><dd class="left"><?php echo L('网站作者'); ?></dd></label>
						<dt class="left"><input id="inputAuthor" class="form-control" type="text" name="author" value="<?php echo $GLOBALS['C']['author']; ?>" placeholder="<?php echo L('网站作者 说明'); ?>" /></dt>
					</dl>
					<dl class="lr">
						<label for="inputKeywords"><dd class="left"><?php echo L('网站关键词'); ?></dd></label>
						<dt class="left"><input id="inputKeywords" class="form-control" type="text" name="keywords" value="<?php echo $GLOBALS['C']['keywords']; ?>" placeholder="<?php echo L('网站关键词 说明'); ?>" /></dt>
					</dl>
					<dl class="lr">
						<label for="inputDescription"><dd class="left"><?php echo L('网站描述'); ?></dd></label>
						<dt class="left">
							<textarea id="inputDescription" class="form-control" rows="5" name="description" placeholder="<?php echo L('网站关键词 说明'); ?>"><?php echo $GLOBALS['C']['description']; ?></textarea>
						</dt>
					</dl>
				</div>
				<div class="row">
					<dl class="lr">
						<dd class="left"><a href="admin.php">&laquo;<?php echo L('返回上级'); ?></a></dd>
						<dt class="left"><input type="submit" class="btn btn-default" name="submit" value="<?php echo L('保存'); ?>" /></dt>
					</dl>
				</div>
			</form>
		</div>

<?php include T('admin_footer', true); ?>