<?php
/*
*	Package:		PHPCrazy
*	Link:			http://zhangyun.org/
*	Author: 		Crazy <mailzhangyun@qq.com>
*	Copyright:		2014-2015 Crazy
*	License:		Please read the LICENSE file.
*/ include T('admin_header', true); ?>
	
			<div class="main">
				<div class="row">
					<h1 class="text-center">设置</h1>
				</div>
<?php foreach ($Module as $k => $v): ?>
				<div class="row">
					<ul>
	<?php foreach ($v as $name => $action): ?>
						<li class="bd-b-line block"><a href="<?php echo AdminUrl($action); ?>" class="block"><h2><?php echo $name; ?></h2></a></li>
	<?php endforeach; ?>
					</ul>
				</div>
<?php endforeach; ?>

<?php include T('admin_footer', true); ?>