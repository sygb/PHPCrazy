<?php
/*
*	Package:		PHPCrazy
*	Link:			http://zhangyun.org/
*	Author: 		Crazy <mailzhangyun@qq.com>
*	Copyright:		2014-2015 Crazy
*	License:		Please read the LICENSE file.
*/ include T('admin_header', true); ?>

		<div class="main">
			<div class="row">
				<h1 class="text-center"><?php echo L($row['username']); ?></h1>
			</div>
			
			<?php if ($submit): include T('error_box'); endif; ?>

			<form action="<?php echo AdminUrl('users&mode=info&user='.$row['id']); ?>" method="post">
				<div class="row">
					<dl class="lr">
						<label for="UserId"><dd class="left"><?php echo L('ID'); ?></dd></label>
						<dt class="left">
							<strong id="UserId"><?php echo $row['id']; ?></strong>
						</dt>	
					</dl>
					<dl class="lr">
						<label for="UserName"><dd class="left"><?php echo L('用户名'); ?></dd></label>
						<dt class="left">
							<input id="UserName" type="text" class="form-control" name="username" value="<?php echo $row['username']; ?>" />
							<p class="text-info text-small"><?php echo L('输入用户名说明'); ?></p>
						</dt>
					</dl>
					<dl class="lr">
						<label for="Email"><dd class="left"><?php echo L('邮箱'); ?></dd></label>
						<dt class="left">
							<input id="Email" type="text" class="form-control" name="email" value="<?php echo $row['email']; ?>" />
						</dt>
					</dl>
					<dl class="lr">
						<label for="auth"><dd class="left"><?php echo L('权限'); ?></dd></label>
						<dt class="left">
							<?php echo Select($GLOBALS['lang']['Auth'], $row['auth'], 'name="auth" id="auth" class="form-control"'); ?>
							<p class="text-info text-small"><?php echo L('修改权限 说明'); ?></p>
						</dt>
					</dl>
					<dl class="lr">
						<label for="password"><dd class="left"><?php echo L('密码'); ?></dd></label>
						<dt class="left">
							<input id="password" class="form-control" type="password" name="password" value="" />
							<p class="text-info text-small"><?php echo L('管理更改密码 说明'); ?></p>
						</dt>
					</dl>
				</div>
				<div class="row">
					<dl class="lr">
						<dd class="left"><a href="<?php echo AdminUrl('users'); ?>">&laquo;<?php echo L('返回上级'); ?></a></dd>
						<dt class="left"><input type="submit" class="btn btn-default" name="submit" value="<?php echo L('保存'); ?>" /></dt>
					</dl>
				</div>
			</form>
		</div>

<?php include T('admin_footer', true); ?>