<?php
/*
*	Package:		PHPCrazy
*	Link:			http://zhangyun.org/
*	Author: 		Crazy <mailzhangyun@qq.com>
*	Copyright:		2014-2015 Crazy
*	License:		Please read the LICENSE file.
*/

if (!defined('INCLUDE_HEADER')):

	define('INCLUDE_HEADER', true);
	
	echo '<?xml version="1.0" encoding="utf-8"?>';
?>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<meta http-equiv="Cache-Control" content="no-cache"/>
		<meta name="author" content="<?php echo $GLOBALS['C']['author']; ?>" />
		<link rel="icon" href="favicon.ico" type="image/x-icon" />
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
		<meta name="keywords" content="<?php echo $GLOBALS['C']['keywords']; ?>"/>
		<meta name="description" content="<?php echo $GLOBALS['C']['description']; ?>" />
		<link rel="stylesheet" type="text/css" href="theme/theme.css" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<title><?php echo $GLOBALS['PageTitle']; ?></title>
	</head>
	<body>
		<div class="header"><a href="<?php echo HomeUrl(); ?>"><img src="<?php echo ThemePath().'images/logo.png'; ?>" /></a></div>
<?php endif; ?>		