<?php
/*
*	这是一个模版文件
*
*	@程序包 Crazy
*	@修订 Crazy v1.0 2015-1-11 02:46
*/
if (!defined('INCLUDE_FOOTER')):

	define('INCLUDE_FOOTER', true);
?>
		<div class="footer">
			<a href="<?php echo HomeUrl(); ?>"><?php echo L('首页'); ?></a> · 
			<a href="<?php echo HomeUrl() . ADMIN_FILE; ?>"><?php echo L('后台'); ?></a> · 
			<a href="../user.php"><?php echo $GLOBALS['U']['username']; ?></a> · 
			<a href="../login.php?action=logout"><?php echo L('注销'); ?></a>
		</div>
	<body>
</html>
<?php endif; ?>