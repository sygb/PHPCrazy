#PHPCrazy
Copyright © 2015，Crazy 版权所有

PHPCrazy 使用 MIT 许可证发布，用户可以自由使用、复制、修改、合并、出版发行、散布、再授权及贩售 PHPCrazy 及其副本。

PHPCrazy 是一个轻量级的开源PHP程序

##实例
###控制层
创建控制层文件 ```world.php``` ,把他放在 ```includes/controller/hello/``` 目录下（没有 ```hello``` 目录请新建一个）, 代码如下:
```php
<?php

// 定义网页标题
$PageTitle = 'hello world';

// 加载 helloworld 模版
include T('helloworld');

?>
```

###视图
创建视图文件 ```helloworld.tpl.php``` ,把他放在 ```themes/Simple/``` 目录下, 代码如下:
```php
<h1>Hello,World!</h1>
```

###访问试试？
```
http://您的域名/index.php/hello:world/
```