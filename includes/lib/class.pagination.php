<?php
/*
*	Package:		PHPCrazy
*	Link:			http://zhangyun.org/
*	Author: 		Crazy <mailzhangyun@qq.com>
*	Copyright:		2014-2015 Crazy
*	License:		Please read the LICENSE file.
*/

/**
* 	分页控制类
*/
class Pagination
{

	var $HTML = array();// 分页模版

	var $php_self;
	var $Urlsuffix = '';

	var $Per;
	var $Start;
	var $Total;

	var $TotalPages;
	var $OnPage;

	/*
	*	new Pagination(Url参数, 统计记录, 开始行, 每页显示行数)
	*/
	public function __construct($Urlsuffix, $Total, $Start, $Per) {
		
		// 取得当前页面地址
		$this->php_self = htmlspecialchars($_SERVER['PHP_SELF'] );

		// URL参数
		$this->Urlsuffix = $Urlsuffix;

		// 记录总数
		$this->Total = abs(intval($Total));

		// 开始行数
		$this->Start = abs(intval($Start));

		// 显示多少行
		$this->Per = abs(intval($Per));

		// 计算出有多少页
		$this->TotalPages = ceil($Total / $Per);

		// 正在打开的页面
		$this->OnPage = floor($Start / $Per) + 1;

		// 分页模板
		$PaginationTpl = ThemePath(true) . 'pagination.tpl.php';

		// 检查分页模板是否存在
		if (!file_exists($PaginationTpl)) {
			
			DEBUG(DEBUG_PHP, L('错误'), sprintf(L('模版 文件 不存在'), 'pagination.html'), __LINE__, __FILE__);

		}

		$html = fread(fopen($PaginationTpl, 'r'), filesize($PaginationTpl));
		$html = str_replace('\\', '\\\\', $html);
		$html = str_replace('\'', '\\\'', $html);
		$html = str_replace("\n", '', $html);		
		
		preg_match_all('#<!-- BEGIN ([0-9a-zA-Z]*?) -->(.*?)<!-- END ([0-9a-zA-Z]*?) -->#', $html, $HTMLArr);

		$eval_str = '';

		foreach ($HTMLArr[1] as $key => $value) {
			
			$eval_str .= "\n" . '$this->HTML[\'' . $value . '\'] = \'' . $HTMLArr[2][$key] .' \';';

		}

		eval($eval_str);

	}

	/*
	*	Pagination::Box()
	*	[首页][上页][下页][末页]
	*/
	public function Box() {

		if ($this->TotalPages <= 0) {

			return '';
		}


		$BoxHtml = str_replace('{FIRST}', $this->First(L('首页')), $this->HTML['Box']);

		$BoxHtml = str_replace('{PREV}', $this->Prev(L('上页')), $BoxHtml);

		$BoxHtml = str_replace('{INFO}', $this->INFO(), $BoxHtml);

		$BoxHtml = str_replace('{NEXT}', $this->Next(L('下页')), $BoxHtml);

		$BoxHtml = str_replace('{LAST}', $this->Last(L('末页')), $BoxHtml);

		return $BoxHtml;

	}

	private function First($Tag) {

		if ($this->TotalPages <= 0) {

			return '';
		}

		if ($this->OnPage == 1) {
			
			return str_replace('{TAG}', $Tag, $this->HTML['Tag']);

		} else {

			$URL = $this->php_self . '?start=0&' . $this->Urlsuffix;

			$First = str_replace('{URL}', $URL, $this->HTML['FirstOrLast']);

			return str_replace('{TAG}', $Tag, $First);

		}

	}

	private function Next($Tag) {

		if ($this->TotalPages <= 0) {

			return '';
		}

		
		if ( $this->OnPage == 1 || ($this->OnPage > 1  && $this->OnPage < $this->TotalPages)) {
			
			$URL = $this->php_self . '?start=' . ($this->OnPage * $this->Per) . '&' . $this->Urlsuffix;

			$Next = str_replace('{URL}', $URL, $this->HTML['NextOrPrev']);

			return str_replace('{TAG}', $Tag, $Next);
		
		} else {

			return str_replace('{TAG}', $Tag, $this->HTML['Tag']);

		}

	}

	private function Prev($Tag) {
		
		if ($this->TotalPages <= 0) {

			return '';
		}

		if ($this->OnPage > 1) {

			$URL = $this->php_self . '?start=' . ( ( $this->OnPage - 2 ) * $this->Per ) . '&' . $this->Urlsuffix;

			$Prev = str_replace('{URL}', $URL, $this->HTML['NextOrPrev']);

			return str_replace('{TAG}', $Tag, $Prev);
			

		} else {

			return str_replace('{TAG}', $Tag, $this->HTML['Tag']);
		}
	}

	private function Last($Tag) {

		if ($this->TotalPages <= 0) {

			return '';
		}

		if ($this->OnPage == $this->TotalPages) {
			
			return str_replace('{TAG}', $Tag, $this->HTML['Tag']);

		} else {

			$URL = $this->php_self . '?start=' . (($this->TotalPages - 1) * $this->Per) . '&' . $this->Urlsuffix;

			$Last = str_replace('{URL}', $URL, $this->HTML['FirstOrLast']);

			return str_replace('{TAG}', $Tag, $Last);

		}

	}

	private function INFO() {


		$Info = str_replace('{ONPAGE}', $this->OnPage, $this->HTML['Info']);

		return str_replace('{TOTAL}', $this->TotalPages, $Info);

	}

}



?>